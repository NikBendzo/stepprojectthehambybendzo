"use strict";

document.addEventListener("DOMContentLoaded", function () {
  const tabsContainer = document.querySelector(".service__tabs");
  const tabContents = document.querySelectorAll(".service__tabs-item");

  tabsContainer.addEventListener("click", function (event) {
    if (event.target.classList.contains("service__tabs-title")) {
      const tabId = event.target.getAttribute("data-tab");

      tabContents.forEach((content) => {
        content.classList.remove("active");
      });

      const activeTab = document.getElementById(tabId);
      activeTab.classList.add("active");

      tabsContainer.querySelectorAll(".service__tabs-title").forEach((tab) => {
        tab.classList.remove("active");
      });

      event.target.classList.add("active");
    }
  });
});

window.addEventListener("DOMContentLoaded", addTegs);
function addTegs() { 
    const galleryItems = document.querySelectorAll(".work .gallery-item");
    const popUp = document.querySelector(".work .pop-up");
    [...galleryItems].slice(1).forEach(li => li.append(popUp.cloneNode(true)));
}

let countDisplayBlock = 12;

const workList = document.querySelector(".work-list");
workList.addEventListener("click", (evt) => {
    const galleryItems = document.querySelectorAll(".work .gallery-item");
    const worksGallery = document.querySelector(".works-gallery")
    let target = evt.target;
    for (let li of workList.children) {
        if (li.matches(".active")) li.classList.remove("active")
    }
    target.classList.add("active");
    let arr = [];                 
    for (let li of galleryItems) {
        li.dataset.typeWork !== target.dataset.typeWork ?
        li.style.display = "none": li.style.display = "block";
        if (target.dataset.typeWork === "all") li.style.display = "block";
        arr.push(window.getComputedStyle(li).display);
    }; 
    countDisplayBlock = arr.filter(display => display.includes("block")).length;
    (countDisplayBlock <= 3 || countDisplayBlock % 4 !== 0) ?
    worksGallery.classList.add("works-gallery-js") :
    worksGallery.classList.remove("works-gallery-js");
});

workList.addEventListener("mouseover", (evt) => {
    const galleryImgs = document.querySelectorAll(".work .gallery-img")
    let target = evt.target;
    if (countDisplayBlock >= 12 && target.tagName === "LI" && target.dataset.typeWork !== "all") {
        for (let img of galleryImgs) {
            if (img.dataset.typeWork !== target.dataset.typeWork) 
            img.style.filter = "brightness(25%)";
        }
    };
});

workList.addEventListener("mouseout", (evt) => {
    const galleryImgs = document.querySelectorAll(".work .gallery-img");
    galleryImgs.forEach(img => img.style.filter = "brightness(100%)");
});

const focusLink = document.querySelector(".hidden-link");          
const btnLoad = document.querySelector(".work .load-btn"); 
btnLoad.addEventListener("click", loadImgs);                                

function loadImgs() {
    btnLoad.classList.remove("bg-btn-add", "bg-btn-min");
    btnLoad.children[0].style.visibility = "visible";            
    const worksGallery = document.querySelector(".works-gallery");       
    let quantLi = worksGallery.children.length;
    setTimeout(() => {                                         
        btnLoad.children[0].style.visibility = "hidden";         
        btnLoad.classList.add("bg-btn-add");                     
        if (quantLi < 25) {                                       
        let factor = quantLi <= 12 ? 0 : 3                                
            for (let i = 1; i <= 12; i += 1) {                  
                let li = worksGallery.children[i-1].cloneNode(true);
                let img = li.children[0];                                 

                if (i <= 3) {                                                             
                    li.setAttribute("data-type-Work", "gd");
                    img.setAttribute("src", `./images/img-base/gd/gd-${i + factor}.jpg`)      
                    img.setAttribute("data-type-Work", "gd");
                } else if (i > 3 && i <= 6) {
                    li.setAttribute("data-type-Work", "wd");
                    img.setAttribute("src", `./images/img-base/wd/wd-${i - 3 + factor}.jpg`) 
                    img.setAttribute("data-type-Work", "wd");
                } else if (i > 6 && i <= 9) {
                    li.setAttribute("data-type-Work", "lp");
                    img.setAttribute("src", `./images/img-base/lp/lp-${i - 6 + factor}.jpg`) 
                    img.setAttribute("data-type-Work", "lp");
                } else if (i > 9 && i <= 12) {
                    li.setAttribute("data-type-Work", "wp");
                    img.setAttribute("src", `./images/img-base/wp/wp-${i - 9 + factor}.jpg`) 
                    img.setAttribute("data-type-Work", "wp");
                }
                worksGallery.append(li);
            }
        };
        if (quantLi >= 24) {                                         
            btnLoad.classList.replace("bg-btn-add", "bg-btn-min");     
            btnLoad.innerHTML = '<i class="loading"></i>MINIMIZE';    
        };
        if (quantLi >= 36) {                                             
            [...worksGallery.children].slice(12).forEach(li => li.remove()) 
            btnLoad.classList.replace("bg-btn-min", "bg-btn-add");       
            btnLoad.innerHTML = '<i class="loading"></i>LOAD MORE'         
            focusLink.click();      
        }
    }, 2000);
};

$(".slider-for").slick({
  slidesToShow: 1,
  slidesToScroll: 1,
  arrows: false,
  fade: true,
  asNavFor: ".slider-nav",
});
$(".slider-nav").slick({
  slidesToShow: 3,
  slidesToScroll: 1,
  asNavFor: ".slider-for",
  dots: true,
  focusOnSelect: true,
  centerMode: true,
  variableWidth: true,
});

function createMasonryElement(counter) {
  const wrapperOfElement = document.createElement("div");

  if (counter % 3 == 0) {
    wrapperOfElement.classList.add(
      "masonry-item",
      "masonry-item--width4",
      "masonary-item-properties"
    );
  } else {
    wrapperOfElement.classList.add(
      "masonry-item",
      "masonry-item--width3",
      "masonary-item-properties"
    );
  }
  wrapperOfElement.append(imageForMasonry(counter));
  wrapperOfElement.append(createWrapperForLinks());

  return wrapperOfElement;
}

function imageForMasonry(elementNumber) {
  const imageForAdding = document.createElement("img");
  imageForAdding.src = `./images/best-images/best-images${elementNumber}.png`;
  imageForAdding.alt = `best-images${elementNumber}`;
  return imageForAdding;
}

function createWrapperForLinks() {
  const wrapperForLinks = document.createElement("div");
  wrapperForLinks.classList.add("wrapper-for-gallery");
  wrapperForLinks.append(createLinkInMasonryElement("search"));
  wrapperForLinks.append(createLinkInMasonryElement("zoom"));
  return wrapperForLinks;
}

function createLinkInMasonryElement(param) {
  const linkSearch = document.createElement("a");
  if (param === "search") {
    linkSearch.classList.add(
      "wrapper-for-gallery__search",
      "wrapper-for-gallery__size",
      "wrapper-for-gallery__background-color"
    );
    linkSearch.href = "#";
  } else {
    linkSearch.classList.add(
      "wrapper-for-gallery__zoom",
      "wrapper-for-gallery__size",
      "wrapper-for-gallery__background-color"
    );
    linkSearch.href = "#";
  }
  linkSearch.append(createLinkWrapper(param));
  return linkSearch;
}

function createLinkWrapper(param) {
  if (param === "search") {
    const searchIcon = document.querySelector('#search-icon');
    const clonedSearchIcon = searchIcon.cloneNode(true); // true означає глибоке клонування
    return clonedSearchIcon;
  }
  if (param === "zoom") {
    const zoomIcon = document.querySelector('#zoom-icon');
    const clonedZoomIcon = zoomIcon.cloneNode(true);
    return clonedZoomIcon;
  }
}

$(document).ready(function () {
  var $grid = $(".masonry-container").imagesLoaded(function () {
    $grid.masonry({
      itemSelector: ".masonry-item",
      columnWidth: 10,
      gutter: 10,
      percentPosition: true,
    });
  });

  $(".button_adding-masonry-item").on("click", function (event) {
    let counter = event.currentTarget.dataset.click;
    counter++;
    event.currentTarget.dataset.click = counter;

    if (event.currentTarget.dataset.click <= 9) {
      var btn = $(this);
      $(btn).buttonLoader("start");
      setTimeout(function () {
        $(btn).buttonLoader("stop");
      }, 1000);
      setTimeout(() => {
        $grid.append(createMasonryElement(counter));
        $grid.masonry("reloadItems");
        $grid.masonry("layout");
      }, 1100);
    }
  });
});

(function ($) {
  $.fn.buttonLoader = function (action) {
    var self = $(this);
    if (action == "start") {
      if ($(self).attr("disabled") == "disabled") {
        e.preventDefault();
      }
      $(".has-spinner").attr("disabled", "disabled");
      $(self).attr("data-btn-text", $(self).text());
      $(self).html(
        '<span class="spinner"><i class="fa fa-spinner fa-spin"></i></span>Loading'
      );
      $(self).addClass("button_adding-masonry-item_change-properties");
      $(self).addClass("active");
      $(self).removeClass("button__add-item");
    }
    if (action == "stop") {
      $(self).html($(self).attr("data-btn-text"));
      $(self).removeClass("active");
      $(".has-spinner").removeAttr("disabled");
      $(self).addClass("button__add-item");
      $(self).removeClass("button_adding-masonry-item_change-properties");
    }
  };
})(jQuery);
